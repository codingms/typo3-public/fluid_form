<?php

$return['fluidform_fluidform'] = [
    'parent' => 'web',
    'position' => [],
    'access' => 'user',
    'iconIdentifier' => 'module-fluid-form',
    'icon' => 'EXT:fluid_form/Resources/Public/Icons/module-fluid-form.svg',
    'path' => '/module/fluidform/fluidform',
    'labels' => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_fluid_form.xlf',
    'extensionName' => 'FluidForm',
    'controllerActions' => [
        \CodingMs\FluidForm\Controller\BackendController::class => [
            'overview',
        ]
    ],
];

return $return;

