<?php

return [
    'dependencies' => ['core', 'backend'],
    'imports' => [
        '@codingms/fluid-form/FluidFormBackend.js' => 'EXT:fluid_form/Resources/Public/JavaScript/Backend/FluidFormBackend.js'
    ],
];
