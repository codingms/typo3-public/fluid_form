<?php

defined('TYPO3') or die();

//
// Page tree icon
if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
        0 => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_label.contains_mails',
        1 => 'mails',
        2 => 'apps-pagetree-folder-contains-mails'
    ];
} else {
    $GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
        'label' => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_label.contains_mails',
        'value' => 'mails',
        'icon' => 'apps-pagetree-folder-contains-mails'
    ];
}
$GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-mails'] = 'apps-pagetree-folder-contains-mails';
