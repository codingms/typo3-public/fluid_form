<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'fluid_form';
$table = 'tx_fluidform_domain_model_form';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;


$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'form_key',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'form_key,form_uid,unique_id,fields,',
        'iconfile' => 'EXT:fluid_form/Resources/Public/Icons/iconmonstr-email-9.svg',
        'typeicon_classes' => ['default' => 'mimetypes-x-content-fluid-form-mail']
    ],
    'types' => [
        '1' => ['showitem' => 'information, --palette--;; form_key_form_uid_unique_id, fields'],
    ],
    'palettes' => [
        'form_key_form_uid_unique_id' => ['showitem' => 'form_key, form_uid, unique_id', 'canNotCollapse' => 1],
    ],
    'columns' => [
        'information' => \CodingMs\AdditionalTca\Tca\Configuration::full('information', $table, $extKey),
        'sys_language_uid' => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'starttime' => [
            'exclude' => 0,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, (int)date('m'), (int)date('d'), (int)date('Y'))
                ],
            ],
        ],
        'endtime' => [
            'exclude' => 0,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, (int)date('m'), (int)date('d'), (int)date('Y'))
                ],
            ],
        ],
        'form_key' => [
            'exclude' => 0,
            'label' => $lll . '.form_key',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string', false, true),
        ],
        'form_uid' => [
            'exclude' => 0,
            'label' => $lll . '.form_uid',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string', false, true),
        ],
        'unique_id' => [
            'exclude' => 0,
            'label' => $lll . '.unique_id',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string', false, true),
        ],
        'fields' => [
            'exclude' => 0,
            'label' => $lll . '.fields',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_fluidform_domain_model_field',
                'foreign_field' => 'fluidform',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
        ],
        'crdate' => [
            'exclude' => 0,
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('dateTime', false, true, '', ['dbType' => 'timestamp']),
        ]
    ],
];

if((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
