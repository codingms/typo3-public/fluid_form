<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'fluid_form';
$table = 'tx_fluidform_domain_model_field';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

if((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $fieldTypeItems = [
        ['Hidden', 'Hidden'],
        ['Input', 'Input'],
        ['Textarea', 'Textarea'],
        ['DateTime', 'DateTime'],
        ['Checkbox', 'Checkbox'],
        ['Select', 'Select'],
        ['Radio', 'Radio'],
        ['Captcha', 'Captcha'],
        ['Upload', 'Upload'],
    ];
} else {
    $fieldTypeItems = [
        ['label'=> 'Hidden', 'value'=> 'Hidden'],
        ['label'=> 'Input', 'value'=> 'Input'],
        ['label'=> 'Textarea', 'value'=> 'Textarea'],
        ['label'=> 'DateTime', 'value'=> 'DateTime'],
        ['label'=> 'Checkbox', 'value'=> 'Checkbox'],
        ['label'=> 'Select', 'value'=> 'Select'],
        ['label'=> 'Radio', 'value'=> 'Radio'],
        ['label'=> 'Captcha', 'value'=> 'Captcha'],
        ['label'=> 'Upload', 'value'=> 'Upload'],
    ];
}

$return = [
    'ctrl' => [
        'title' => 'LLL:EXT:fluid_form/Resources/Private/Language/locallang_db.xlf:tx_fluidform_domain_model_field',
        'label' => 'field_label',
        'label_alt' => 'field_value',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'field_type,field_label,field_key,field_upload,field_value,field_text,',
        'iconfile' => 'EXT:fluid_form/Resources/Public/Icons/iconmonstr-email-9.svg',
        'typeicon_classes' => ['default' => 'mimetypes-x-content-fluid-form-field']
    ],
    'types' => [
        '1' => [
            'showitem' => 'information, --palette--;; label_key_type, field_upload, field_value, field_text'
        ],
    ],
    'palettes' => [
        'label_key_type' => ['showitem' => 'field_label, field_key, field_type', 'canNotCollapse' => 1],
    ],
    'columns' => [
        'information' => \CodingMs\AdditionalTca\Tca\Configuration::full('information', $table, $extKey),
        'sys_language_uid' => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\AdditionalTca\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\AdditionalTca\Tca\Configuration::full('endtime'),
        'field_type' => [
            'exclude' => 0,
            'label' => $lll . '.field_type',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 'default',
                'items' => $fieldTypeItems,
                'readOnly' => 1,
            ],
        ],
        'field_label' => [
            'exclude' => 0,
            'label' => $lll . '.field_label',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string', false, true),
        ],
        'field_key' => [
            'exclude' => 0,
            'label' => $lll . '.field_key',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string', false, true),
        ],
        'field_upload' => [
            'exclude' => 0,
            'label' => $lll . '.field_upload',
            'displayCond' => 'FIELD:field_type:=:Upload',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'field_upload',
                [
                    'maxitems' => 1,
                    'foreign_match_fields' => [
                        'fieldname' => 'field_upload',
                        'tablenames' => 'tx_fluidform_domain_model_field',
                    ],
                ],
                '*'
            /**
             * @todo: needs an restriction!
             */
            ),
        ],
        'field_value' => [
            'exclude' => 0,
            'label' => $lll . '.field_value',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 5,
                'eval' => 'trim',
                'readOnly' => 1,
            ],
            'defaultExtras' => 'fixed-font:enable-tab',
        ],
        'field_text' => [
            'exclude' => 0,
            'label' => $lll . '.field_text',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string', false, true),
        ],
        'fluidform' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];

if((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
    $return['columns']['field_upload']['config']['foreign_match_fields']['table_local'] = 'sys_file';
}
return $return;
