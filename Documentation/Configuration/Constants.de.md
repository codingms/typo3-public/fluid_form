# TypoScript-Konstanten Einstellungen


## Allgemein

| **Konstante**    | plugin.tx_fluidform.view.templateRootPath                                                                          |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Pfad zu Template (FE)                                                                                              |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_form/Resources/Private/Templates/                                                                        |

| **Konstante**    | plugin.tx_fluidform.view.partialRootPath                                                                           |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Pfad zu Template-Partials (FE)                                                                                     |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_form/Resources/Private/Partials/                                                                         |

| **Konstante**    | plugin.tx_fluidform.view.layoutRootPath                                                                            |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Pfad zu Template-Layout (FE)                                                                                       |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_form/Resources/Private/Layouts/                                                                          |

| **Konstante**    | plugin.tx_fluidform.persistence.storagePid                                                                         |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Standard Datensatz Container                                                                                       |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | string                                                                                                             |
| **Standardwert** |                                                                                                                    |

| **Konstante**    | themes.configuration.colors.primary                                                                                |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Primärfarbe                                                                                                        |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | color                                                                                                              |
| **Standardwert** | #8DD02E                                                                                                            |



## Fluid-Form View

| **Konstante**    | themes.configuration.extension.fluid_form.view.templateRootPath                                                    |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Pfad zu Template (FE)                                                                                              |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_form/Resources/Private/Templates/                                                                        |

| **Konstante**    | themes.configuration.extension.fluid_form.view.partialRootPath                                                     |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Pfad zu Template-Partials (FE)                                                                                     |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_form/Resources/Private/Partials/                                                                         |

| **Konstante**    | themes.configuration.extension.fluid_form.view.layoutRootPath                                                      |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Pfad zu Template-Layout (FE)                                                                                       |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_form/Resources/Private/Layouts/                                                                          |



## Fluid-Form PDF

| **Konstante**    | themes.configuration.extension.fluid_form.pdf.receiver.fontPath                                                    |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Empfänger Font Pfad                                                                                                |
| **Beschreibung** | Überschreibt Pfad zu PDF-Fonts                                                                                     |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_fpdf/Resources/Private/Fonts                                                                             |

| **Konstante**    | themes.configuration.extension.fluid_form.pdf.receiver.imagePath                                                   |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Empfänger Bild Pfad                                                                                                |
| **Beschreibung** | Überschreibt Pfad zu PDF-Bilder                                                                                    |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_fpdf/Resources/Private/Images                                                                            |

| **Konstante**    | themes.configuration.extension.fluid_form.pdf.receiver.background                                                  |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Empfänger Hintergrund Pfad                                                                                         |
| **Beschreibung** | Beispiel EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf oder fileadmin/Pdf/Expose/Briefpapier.pdf |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf                                                    |

| **Konstante**    | themes.configuration.extension.fluid_form.pdf.receiver.partialRootPath                                             |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Empfänger Partials Pfad                                                                                            |
| **Beschreibung** | Überschreibt Pfad zu Fluid-Partials                                                                                |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_form/Resources/Private/Partials/                                                                         |

| **Konstante**    | themes.configuration.extension.fluid_form.pdf.receiver.templateRootPath                                            |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Empfänger Template Pfad                                                                                            |
| **Beschreibung** | überschreibt Pfad zu Expose-Template                                                                               |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_form/Resources/Private/Templates/                                                                        |

| **Konstante**    | themes.configuration.extension.fluid_form.pdf.sender.fontPath                                                      |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Sender Font Pfad                                                                                                   |
| **Beschreibung** | überschreibt Pfad zu PDF-Fonts                                                                                     |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_fpdf/Resources/Private/Fonts                                                                             |

| **Konstante**    | themes.configuration.extension.fluid_form.pdf.sender.imagePath                                                     |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Sender Bild Pfad                                                                                                   |
| **Beschreibung** | überschreibt Pfad zu PDF-Bilder                                                                                    |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_fpdf/Resources/Private/Images                                                                            |

| **Konstante**    | themes.configuration.extension.fluid_form.pdf.sender.background                                                    |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Sender Hintergrund Pfad                                                                                            |
| **Beschreibung** | Beispiel EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf oder fileadmin/Pdf/Expose/Briefpapier.pdf |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf                                                    |

| **Konstante**    | themes.configuration.extension.fluid_form.pdf.sender.partialRootPath                                               |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Sender Partials Pfad                                                                                               |
| **Beschreibung** | überschreibt Pfad zu Fluid-Partials                                                                                |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_form/Resources/Private/Partials/                                                                         |

| **Konstante**    | themes.configuration.extension.fluid_form.pdf.sender.templateRootPath                                              |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Sender Template Pfad                                                                                               |
| **Beschreibung** | Überschreibt Pfad zu Expose-Template                                                                               |
| **Typ**          | string                                                                                                             |
| **Standardwert** | EXT:fluid_form/Resources/Private/Templates/                                                                        |



## Datei Upload Formular

| **Konstante**    | themes.configuration.extension.fluid_form.fileUpload.mail.active                                                   |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail-Versand activieren                                                                                            |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                                                                      |
| **Standardwert** | 1                                                                                                                  |

| **Konstante**    | themes.configuration.extension.fluid_form.fileUpload.mail.subject                                                  |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail Betreff                                                                                                       |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | string                                                                                                             |
| **Standardwert** | Fluid-Form: File-Upload empfangen                                                                                  |

| **Konstante**    | themes.configuration.extension.fluid_form.fileUpload.mail.from.name                                                |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail von                                                                                                           |
| **Beschreibung** | Name des Absenders                                                                                                 |
| **Typ**          | string                                                                                                             |
| **Standardwert** | Fluid-Form                                                                                                         |

| **Konstante**    | themes.configuration.extension.fluid_form.fileUpload.mail.from.email                                               |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail von                                                                                                           |
| **Beschreibung** | E-Mail des Absenders                                                                                               |
| **Typ**          | string                                                                                                             |
| **Standardwert** | fluid-form@t3co.de                                                                                                 |

| **Konstante**    | themes.configuration.extension.fluid_form.fileUpload.mail.to.0.name                                                |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail an                                                                                                            |
| **Beschreibung** | Name des ersten Empfängers                                                                                         |
| **Typ**          | string                                                                                                             |
| **Standardwert** | Fluid form                                                                                                         |

| **Konstante**    | themes.configuration.extension.fluid_form.fileUpload.mail.to.0.email                                               |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail an                                                                                                            |
| **Beschreibung** | E-Mail vom ersten Empfänger                                                                                        |
| **Typ**          | string                                                                                                             |
| **Standardwert** | fluid-form-to0@t3co.de                                                                                             |

| **Konstante**    | themes.configuration.extension.fluid_form.fileUpload.database.active                                               |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Speichern von Mails in der Datenbank aktivierien                                                                   |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                                                                      |
| **Standardwert** | 0                                                                                                                  |

| **Konstante**    | themes.configuration.extension.fluid_form.fileUpload.database.storagePid                                           |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Storage Container Mails im Datenbank                                                                               |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | int+                                                                                                               |
| **Standardwert** | 0                                                                                                                  |

| **Konstante**    | themes.configuration.extension.fluid_form.fileUpload.upload.active                                                 |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Upload-Finisher aktivieren (nur erforderlich, wenn Datei-Uploads vervwendet sind)                                  |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                                                                      |
| **Standardwert** | 1                                                                                                                  |



## Bewerbungsformular

| **Konstante**    | themes.configuration.extension.fluid_form.jobApplication.mail.active                                               |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail-Versand activieren                                                                                            |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                                                                      |
| **Standardwert** | 1                                                                                                                  |

| **Konstante**    | themes.configuration.extension.fluid_form.jobApplication.mail.subject                                              |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail Betreff                                                                                                       |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | string                                                                                                             |
| **Standardwert** | Fluid-Form: Bewerbung                                                                                              |

| **Konstante**    | themes.configuration.extension.fluid_form.jobApplication.mail.from.name                                            |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail von                                                                                                           |
| **Beschreibung** | Name des Absenders                                                                                                 |
| **Typ**          | string                                                                                                             |
| **Standardwert** | Fluid-Form                                                                                                         |

| **Konstante**    | themes.configuration.extension.fluid_form.jobApplication.mail.from.email                                           |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail von                                                                                                           |
| **Beschreibung** | E-Mail des Absenders                                                                                               |
| **Typ**          | string                                                                                                             |
| **Standardwert** | fluid-form@t3co.de                                                                                                 |

| **Konstante**    | themes.configuration.extension.fluid_form.jobApplication.mail.to.0.name                                            |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail an                                                                                                            |
| **Beschreibung** | Name des ersten Empfängers                                                                                         |
| **Typ**          | string                                                                                                             |
| **Standardwert** | Fluid form                                                                                                         |

| **Konstante**    | themes.configuration.extension.fluid_form.jobApplication.mail.to.0.email                                           |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail an                                                                                                            |
| **Beschreibung** | E-Mail vom ersten Empfänger                                                                                        |
| **Typ**          | string                                                                                                             |
| **Standardwert** | fluid-form-to0@t3co.de                                                                                             |

| **Konstante**    | themes.configuration.extension.fluid_form.jobApplication.database.active                                           |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Speichern von Mails in der Datenbank aktivierien                                                                   |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                                                                      |
| **Standardwert** | 0                                                                                                                  |

| **Konstante**    | themes.configuration.extension.fluid_form.jobApplication.database.storagePid                                       |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Storage Container Mails im Datenbank                                                                               |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | int+                                                                                                               |
| **Standardwert** | 0                                                                                                                  |

| **Konstante**    | themes.configuration.extension.fluid_form.jobApplication.upload.active                                             |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Upload-Finisher aktivieren (nur erforderlich, wenn Datei-Uploads vervwendet sind)                                  |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                                                                      |
| **Standardwert** | 1                                                                                                                  |



## Kontakt Basis Formular

| **Konstante**    | themes.configuration.extension.fluid_form.contactBasic.mail.active                                                 |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail-Versand activieren                                                                                            |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                                                                      |
| **Standardwert** | 1                                                                                                                  |

| **Konstante**    | themes.configuration.extension.fluid_form.contactBasic.mail.subject                                                |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail Betreff                                                                                                       |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | string                                                                                                             |
| **Standardwert** | Fluid-Form: Kontakt-Anfrage                                                                                        |

| **Konstante**    | themes.configuration.extension.fluid_form.contactBasic.mail.from.name                                              |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail von                                                                                                           |
| **Beschreibung** | Name des Absenders                                                                                                 |
| **Typ**          | string                                                                                                             |
| **Standardwert** | Fluid-Form                                                                                                         |

| **Konstante**    | themes.configuration.extension.fluid_form.contactBasic.mail.from.email                                             |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail von                                                                                                           |
| **Beschreibung** | E-Mail des Absenders                                                                                               |
| **Typ**          | string                                                                                                             |
| **Standardwert** | fluid-form@t3co.de                                                                                                 |

| **Konstante**    | themes.configuration.extension.fluid_form.contactBasic.mail.to.0.name                                              |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail an                                                                                                            |
| **Beschreibung** | Name des ersten Empfängers                                                                                         |
| **Typ**          | string                                                                                                             |
| **Standardwert** | Fluid form                                                                                                         |

| **Konstante**    | themes.configuration.extension.fluid_form.contactBasic.mail.to.0.email                                             |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail an                                                                                                            |
| **Beschreibung** | E-Mail vom ersten Empfänger                                                                                        |
| **Typ**          | string                                                                                                             |
| **Standardwert** | fluid-form-to0@t3co.de                                                                                             |

| **Konstante**    | themes.configuration.extension.fluid_form.contactBasic.database.active                                             |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Speichern von Mails in der Datenbank aktivierien                                                                   |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                                                                      |
| **Standardwert** | 0                                                                                                                  |

| **Konstante**    | themes.configuration.extension.fluid_form.contactBasic.database.storagePid                                         |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Storage Container Mails im Datenbank                                                                               |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | int+                                                                                                               |
| **Standardwert** | 0                                                                                                                  |

| **Konstante**    | themes.configuration.extension.fluid_form.contactBasic.upload.active                                               |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Upload-Finisher aktivieren (nur erforderlich, wenn Datei-Uploads verwendet sind)                                   |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                                                                      |
| **Standardwert** | 0                                                                                                                  |



## Rückrufformular

| **Konstante**    | themes.configuration.extension.fluid_form.callBack.mail.active                                                     |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail-Versand activieren                                                                                            |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                                                                      |
| **Standardwert** | 1                                                                                                                  |

| **Konstante**    | themes.configuration.extension.fluid_form.callBack.mail.subject                                                    |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail Betreff                                                                                                       |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | string                                                                                                             |
| **Standardwert** | Fluid-Form: Rückruf-Anfrage                                                                                        |

| **Konstante**    | themes.configuration.extension.fluid_form.callBack.mail.from.name                                                  |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail von                                                                                                           |
| **Beschreibung** | Name des Absenders                                                                                                 |
| **Typ**          | string                                                                                                             |
| **Standardwert** | Fluid-Form                                                                                                         |

| **Konstante**    | themes.configuration.extension.fluid_form.callBack.mail.from.email                                                 |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail von                                                                                                           |
| **Beschreibung** | E-Mail des Absenders                                                                                               |
| **Typ**          | string                                                                                                             |
| **Standardwert** | fluid-form@t3co.de                                                                                                 |

| **Konstante**    | themes.configuration.extension.fluid_form.callBack.mail.to.0.name                                                  |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail an                                                                                                            |
| **Beschreibung** | Name des ersten Empfängers                                                                                         |
| **Typ**          | string                                                                                                             |
| **Standardwert** | Fluid form                                                                                                         |

| **Konstante**    | themes.configuration.extension.fluid_form.callBack.mail.to.0.email                                                 |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Mail an                                                                                                            |
| **Beschreibung** | E-Mail vom ersten Empfänger                                                                                        |
| **Typ**          | string                                                                                                             |
| **Standardwert** | fluid-form-to0@t3co.de                                                                                             |

| **Konstante**    | themes.configuration.extension.fluid_form.callBack.database.active                                                 |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Speichern von Mails in der Datenbank aktivierien                                                                   |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                                                                      |
| **Standardwert** | 0                                                                                                                  |

| **Konstante**    | themes.configuration.extension.fluid_form.callBack.database.storagePid                                             |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Storage Container Mails im Datenbank                                                                               |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | int+                                                                                                               |
| **Standardwert** | 0                                                                                                                  |

| **Konstante**    | themes.configuration.extension.fluid_form.callBack.upload.active                                                   |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------|
| **Label**        | Upload-Finisher aktivieren (nur erforderlich, wenn Datei-Uploads vervwendet sind)                                  |
| **Beschreibung** |                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                                                                      |
| **Standardwert** | 0                                                                                                                  |



