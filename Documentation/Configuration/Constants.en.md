# TypoScript constant settings


## General

| **Constant**      | plugin.tx_fluidform.view.templateRootPath                                                                           |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Path to templates (FE)                                                                                              |
| **Description**   |                                                                                                                     |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_form/Resources/Private/Templates/                                                                         |

| **Constant**      | plugin.tx_fluidform.view.partialRootPath                                                                            |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Path to template partials (FE)                                                                                      |
| **Description**   |                                                                                                                     |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_form/Resources/Private/Partials/                                                                          |

| **Constant**      | plugin.tx_fluidform.view.layoutRootPath                                                                             |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Path to template layouts (FE)                                                                                       |
| **Description**   |                                                                                                                     |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_form/Resources/Private/Layouts/                                                                           |

| **Constant**      | plugin.tx_fluidform.persistence.storagePid                                                                          |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Default storage PID                                                                                                 |
| **Description**   |                                                                                                                     |
| **Type**          | string                                                                                                              |
| **Default value** |                                                                                                                     |

| **Constant**      | themes.configuration.colors.primary                                                                                 |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Primary color                                                                                                       |
| **Description**   |                                                                                                                     |
| **Type**          | color                                                                                                               |
| **Default value** | #8DD02E                                                                                                             |



## Fluid-Form View

| **Constant**      | themes.configuration.extension.fluid_form.view.templateRootPath                                                     |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Path to templates (FE)                                                                                              |
| **Description**   |                                                                                                                     |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_form/Resources/Private/Templates/                                                                         |

| **Constant**      | themes.configuration.extension.fluid_form.view.partialRootPath                                                      |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Path to template partials (FE)                                                                                      |
| **Description**   |                                                                                                                     |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_form/Resources/Private/Partials/                                                                          |

| **Constant**      | themes.configuration.extension.fluid_form.view.layoutRootPath                                                       |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Path to template layouts (FE)                                                                                       |
| **Description**   |                                                                                                                     |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_form/Resources/Private/Layouts/                                                                           |



## Fluid-Form PDF

| **Constant**      | themes.configuration.extension.fluid_form.pdf.receiver.fontPath                                                     |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Receiver Font path                                                                                                  |
| **Description**   | Overwrites the path to the PDF-Fonts                                                                                |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_fpdf/Resources/Private/Fonts                                                                              |

| **Constant**      | themes.configuration.extension.fluid_form.pdf.receiver.imagePath                                                    |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Receiver Image path                                                                                                 |
| **Description**   | Overwrites the path to the PDF-Images                                                                               |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_fpdf/Resources/Private/Images                                                                             |

| **Constant**      | themes.configuration.extension.fluid_form.pdf.receiver.background                                                   |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Receiver Background path                                                                                            |
| **Description**   | For example EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf or fileadmin/Pdf/Expose/Briefpapier.pdf |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf                                                     |

| **Constant**      | themes.configuration.extension.fluid_form.pdf.receiver.partialRootPath                                              |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Receiver Partials path                                                                                              |
| **Description**   | Overwrites the path to the Fluid-Partials                                                                           |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_form/Resources/Private/Partials/                                                                          |

| **Constant**      | themes.configuration.extension.fluid_form.pdf.receiver.templateRootPath                                             |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Receiver Template path                                                                                              |
| **Description**   | Overwrites the path to the Expose-Template                                                                          |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_form/Resources/Private/Templates/                                                                         |

| **Constant**      | themes.configuration.extension.fluid_form.pdf.sender.fontPath                                                       |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Sender Font path                                                                                                    |
| **Description**   | Overwrites the path to the PDF-Fonts                                                                                |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_fpdf/Resources/Private/Fonts                                                                              |

| **Constant**      | themes.configuration.extension.fluid_form.pdf.sender.imagePath                                                      |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Sender Image path                                                                                                   |
| **Description**   | Overwrites the path to the PDF-Images                                                                               |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_fpdf/Resources/Private/Images                                                                             |

| **Constant**      | themes.configuration.extension.fluid_form.pdf.sender.background                                                     |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Sender Background path                                                                                              |
| **Description**   | For example EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf or fileadmin/Pdf/Expose/Briefpapier.pdf |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf                                                     |

| **Constant**      | themes.configuration.extension.fluid_form.pdf.sender.partialRootPath                                                |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Sender Partials path                                                                                                |
| **Description**   | Overwrites the path to the Fluid-Partials                                                                           |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_form/Resources/Private/Partials/                                                                          |

| **Constant**      | themes.configuration.extension.fluid_form.pdf.sender.templateRootPath                                               |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Sender Template path                                                                                                |
| **Description**   | Overwrites the path to the Expose-Template                                                                          |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_form/Resources/Private/Templates/                                                                         |



## File Upload form

| **Constant**      | themes.configuration.extension.fluid_form.fileUpload.mail.active                                                    |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Activate sending mails in                                                                                           |
| **Description**   |                                                                                                                     |
| **Type**          | Selectbox with options: 0, 1                                                                                        |
| **Default value** | 1                                                                                                                   |

| **Constant**      | themes.configuration.extension.fluid_form.fileUpload.mail.subject                                                   |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail subject                                                                                                        |
| **Description**   |                                                                                                                     |
| **Type**          | string                                                                                                              |
| **Default value** | Fluid-Form: File-Upload empfangen                                                                                   |

| **Constant**      | themes.configuration.extension.fluid_form.fileUpload.mail.from.name                                                 |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail from                                                                                                           |
| **Description**   | Name from the sender                                                                                                |
| **Type**          | string                                                                                                              |
| **Default value** | Fluid-Form                                                                                                          |

| **Constant**      | themes.configuration.extension.fluid_form.fileUpload.mail.from.email                                                |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail from                                                                                                           |
| **Description**   | Email from the sender                                                                                               |
| **Type**          | string                                                                                                              |
| **Default value** | fluid-form@t3co.de                                                                                                  |

| **Constant**      | themes.configuration.extension.fluid_form.fileUpload.mail.to.0.name                                                 |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail to                                                                                                             |
| **Description**   | Name from the first receiver                                                                                        |
| **Type**          | string                                                                                                              |
| **Default value** | Fluid form                                                                                                          |

| **Constant**      | themes.configuration.extension.fluid_form.fileUpload.mail.to.0.email                                                |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail to                                                                                                             |
| **Description**   | Email from the first receiver                                                                                       |
| **Type**          | string                                                                                                              |
| **Default value** | fluid-form-to0@t3co.de                                                                                              |

| **Constant**      | themes.configuration.extension.fluid_form.fileUpload.database.active                                                |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Activate saving mails in database                                                                                   |
| **Description**   |                                                                                                                     |
| **Type**          | Selectbox with options: 0, 1                                                                                        |
| **Default value** | 0                                                                                                                   |

| **Constant**      | themes.configuration.extension.fluid_form.fileUpload.database.storagePid                                            |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Storage container mails in database                                                                                 |
| **Description**   |                                                                                                                     |
| **Type**          | int+                                                                                                                |
| **Default value** | 0                                                                                                                   |

| **Constant**      | themes.configuration.extension.fluid_form.fileUpload.upload.active                                                  |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Activate upload finisher (only required when you're using file uploads)                                             |
| **Description**   |                                                                                                                     |
| **Type**          | Selectbox with options: 0, 1                                                                                        |
| **Default value** | 1                                                                                                                   |



## Job application form

| **Constant**      | themes.configuration.extension.fluid_form.jobApplication.mail.active                                                |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Activate sending mails in                                                                                           |
| **Description**   |                                                                                                                     |
| **Type**          | Selectbox with options: 0, 1                                                                                        |
| **Default value** | 1                                                                                                                   |

| **Constant**      | themes.configuration.extension.fluid_form.jobApplication.mail.subject                                               |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail subject                                                                                                        |
| **Description**   |                                                                                                                     |
| **Type**          | string                                                                                                              |
| **Default value** | Fluid-Form: Bewerbung                                                                                               |

| **Constant**      | themes.configuration.extension.fluid_form.jobApplication.mail.from.name                                             |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail from                                                                                                           |
| **Description**   | Name from the sender                                                                                                |
| **Type**          | string                                                                                                              |
| **Default value** | Fluid-Form                                                                                                          |

| **Constant**      | themes.configuration.extension.fluid_form.jobApplication.mail.from.email                                            |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail from                                                                                                           |
| **Description**   | Email from the sender                                                                                               |
| **Type**          | string                                                                                                              |
| **Default value** | fluid-form@t3co.de                                                                                                  |

| **Constant**      | themes.configuration.extension.fluid_form.jobApplication.mail.to.0.name                                             |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail to                                                                                                             |
| **Description**   | Name from the first receiver                                                                                        |
| **Type**          | string                                                                                                              |
| **Default value** | Fluid form                                                                                                          |

| **Constant**      | themes.configuration.extension.fluid_form.jobApplication.mail.to.0.email                                            |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail to                                                                                                             |
| **Description**   | Email from the first receiver                                                                                       |
| **Type**          | string                                                                                                              |
| **Default value** | fluid-form-to0@t3co.de                                                                                              |

| **Constant**      | themes.configuration.extension.fluid_form.jobApplication.database.active                                            |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Activate saving mails in database                                                                                   |
| **Description**   |                                                                                                                     |
| **Type**          | Selectbox with options: 0, 1                                                                                        |
| **Default value** | 0                                                                                                                   |

| **Constant**      | themes.configuration.extension.fluid_form.jobApplication.database.storagePid                                        |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Storage container mails in database                                                                                 |
| **Description**   |                                                                                                                     |
| **Type**          | int+                                                                                                                |
| **Default value** | 0                                                                                                                   |

| **Constant**      | themes.configuration.extension.fluid_form.jobApplication.upload.active                                              |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Activate upload finisher (only required when you're using file uploads)                                             |
| **Description**   |                                                                                                                     |
| **Type**          | Selectbox with options: 0, 1                                                                                        |
| **Default value** | 1                                                                                                                   |



## Contact basic form

| **Constant**      | themes.configuration.extension.fluid_form.contactBasic.mail.active                                                  |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Activate sending mails in                                                                                           |
| **Description**   |                                                                                                                     |
| **Type**          | Selectbox with options: 0, 1                                                                                        |
| **Default value** | 1                                                                                                                   |

| **Constant**      | themes.configuration.extension.fluid_form.contactBasic.mail.subject                                                 |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail subject                                                                                                        |
| **Description**   |                                                                                                                     |
| **Type**          | string                                                                                                              |
| **Default value** | Fluid-Form: Kontakt-Anfrage                                                                                         |

| **Constant**      | themes.configuration.extension.fluid_form.contactBasic.mail.from.name                                               |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail from                                                                                                           |
| **Description**   | Name from the sender                                                                                                |
| **Type**          | string                                                                                                              |
| **Default value** | Fluid-Form                                                                                                          |

| **Constant**      | themes.configuration.extension.fluid_form.contactBasic.mail.from.email                                              |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail from                                                                                                           |
| **Description**   | Email from the sender                                                                                               |
| **Type**          | string                                                                                                              |
| **Default value** | fluid-form@t3co.de                                                                                                  |

| **Constant**      | themes.configuration.extension.fluid_form.contactBasic.mail.to.0.name                                               |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail to                                                                                                             |
| **Description**   | Name from the first receiver                                                                                        |
| **Type**          | string                                                                                                              |
| **Default value** | Fluid form                                                                                                          |

| **Constant**      | themes.configuration.extension.fluid_form.contactBasic.mail.to.0.email                                              |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail to                                                                                                             |
| **Description**   | Email from the first receiver                                                                                       |
| **Type**          | string                                                                                                              |
| **Default value** | fluid-form-to0@t3co.de                                                                                              |

| **Constant**      | themes.configuration.extension.fluid_form.contactBasic.database.active                                              |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Activate saving mails in database                                                                                   |
| **Description**   |                                                                                                                     |
| **Type**          | Selectbox with options: 0, 1                                                                                        |
| **Default value** | 0                                                                                                                   |

| **Constant**      | themes.configuration.extension.fluid_form.contactBasic.database.storagePid                                          |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Storage container mails in database                                                                                 |
| **Description**   |                                                                                                                     |
| **Type**          | int+                                                                                                                |
| **Default value** | 0                                                                                                                   |

| **Constant**      | themes.configuration.extension.fluid_form.contactBasic.upload.active                                                |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Activate upload finisher (only required when you're using file uploads)                                             |
| **Description**   |                                                                                                                     |
| **Type**          | Selectbox with options: 0, 1                                                                                        |
| **Default value** | 0                                                                                                                   |



## Call back form

| **Constant**      | themes.configuration.extension.fluid_form.callBack.mail.active                                                      |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Activate sending mails in                                                                                           |
| **Description**   |                                                                                                                     |
| **Type**          | Selectbox with options: 0, 1                                                                                        |
| **Default value** | 1                                                                                                                   |

| **Constant**      | themes.configuration.extension.fluid_form.callBack.mail.subject                                                     |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail subject                                                                                                        |
| **Description**   |                                                                                                                     |
| **Type**          | string                                                                                                              |
| **Default value** | Fluid-Form: Rückruf-Anfrage                                                                                         |

| **Constant**      | themes.configuration.extension.fluid_form.callBack.mail.from.name                                                   |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail from                                                                                                           |
| **Description**   | Name from the sender                                                                                                |
| **Type**          | string                                                                                                              |
| **Default value** | Fluid-Form                                                                                                          |

| **Constant**      | themes.configuration.extension.fluid_form.callBack.mail.from.email                                                  |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail from                                                                                                           |
| **Description**   | Email from the sender                                                                                               |
| **Type**          | string                                                                                                              |
| **Default value** | fluid-form@t3co.de                                                                                                  |

| **Constant**      | themes.configuration.extension.fluid_form.callBack.mail.to.0.name                                                   |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail to                                                                                                             |
| **Description**   | Name from the first receiver                                                                                        |
| **Type**          | string                                                                                                              |
| **Default value** | Fluid form                                                                                                          |

| **Constant**      | themes.configuration.extension.fluid_form.callBack.mail.to.0.email                                                  |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Mail to                                                                                                             |
| **Description**   | Email from the first receiver                                                                                       |
| **Type**          | string                                                                                                              |
| **Default value** | fluid-form-to0@t3co.de                                                                                              |

| **Constant**      | themes.configuration.extension.fluid_form.callBack.database.active                                                  |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Activate saving mails in database                                                                                   |
| **Description**   |                                                                                                                     |
| **Type**          | Selectbox with options: 0, 1                                                                                        |
| **Default value** | 0                                                                                                                   |

| **Constant**      | themes.configuration.extension.fluid_form.callBack.database.storagePid                                              |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Storage container mails in database                                                                                 |
| **Description**   |                                                                                                                     |
| **Type**          | int+                                                                                                                |
| **Default value** | 0                                                                                                                   |

| **Constant**      | themes.configuration.extension.fluid_form.callBack.upload.active                                                    |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Activate upload finisher (only required when you're using file uploads)                                             |
| **Description**   |                                                                                                                     |
| **Type**          | Selectbox with options: 0, 1                                                                                        |
| **Default value** | 0                                                                                                                   |



