define([
    'jquery',
    'TYPO3/CMS/Core/DocumentService',
    'TYPO3/CMS/Backend/DateTimePicker'
    ],
    function (
        jQuery,
        DocumentService,
        DateTimePicker
    ) {

        const FluidFormBackendLegacy = {};

        FluidFormBackendLegacy.initialize = () => {
            let elems = document.querySelectorAll('.t3js-datetimepicker'),
                array = jQuery.makeArray(elems);
            jQuery.each(array.valueOf(), function (i, el) {
                DateTimePicker.initialize(el);
                const $hidden = jQuery(el).closest('.input-group').find('input[type="hidden"]');
                const $visible = jQuery(el);
                const values = FluidFormBackendLegacy.generateValues(new Date($visible.val()))
                $hidden.val(values.hidden);
                $visible.val(values.visible);

                $visible.on('change', function(e){
                    setTimeout(function(){
                        const values = FluidFormBackendLegacy.generateValues(new Date($hidden.val()));
                        $hidden.val(values.hidden);
                    });
                })
            });
        }

        /**
         * Prepends 0 to the numbers that are less 10.
         *
         * @param {Number} value
         * @returns {String}
         */
        FluidFormBackendLegacy.formatWithZeroes = (value) => {
            return value > 9 ? value : `0${value}`;
        }

        /**
         * Generates string values for inserting to visible and hidden TYPO3 inputs.
         *
         * @param {Date} date
         * @returns {{visible: string, hidden: string}}
         */
        FluidFormBackendLegacy.generateValues = (date) => {
            const year = date.getFullYear();
            const month = FluidFormBackendLegacy.formatWithZeroes(date.getMonth() + 1);
            const day = FluidFormBackendLegacy.formatWithZeroes(date.getDate());
            return {
                hidden: `${year}-${month}-${day}`,
                visible: `${day}-${month}-${year}`
            };
        }

        return FluidFormBackendLegacy;
});
