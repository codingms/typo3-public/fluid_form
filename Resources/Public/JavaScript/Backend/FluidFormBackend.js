import DocumentService from "@typo3/core/document-service.js";
import DateTimePicker from "@typo3/backend/date-time-picker.js";
import jQuery from "jquery";

class FluidFormBackend {

	initialize() {
        DocumentService.ready().then(() => {
            const _this = this;
            let elems = document.querySelectorAll('.t3js-datetimepicker'),
                array = jQuery.makeArray(elems);
            jQuery.each(array.valueOf(), function (i, el) {
                DateTimePicker.initialize(el);
                const $hidden = jQuery(el).closest('.input-group').find('input[type="hidden"]');
                const $visible = jQuery(el);
                const values = _this.generateValues(new Date($visible.val()))
                $hidden.val(values.hidden);
                $visible.val(values.visible);

                $visible.on('change', function(e){
                    setTimeout(function(){
                        const values = _this.generateValues(new Date($hidden.val()));
                        $hidden.val(values.hidden);
                    });
                })
            });
        });
	}

    /**
     * Prepends 0 to the numbers that are less 10.
     *
     * @param {Number} value
     * @returns {String}
     */
    formatWithZeroes(value) {
        return value > 9 ? value : `0${value}`;
    }

    /**
     * Generates string values for inserting to visible and hidden TYPO3 inputs.
     *
     * @param {Date} date
     * @returns {{visible: string, hidden: string}}
     */
    generateValues(date) {
        const year = date.getFullYear();
        const month = this.formatWithZeroes(date.getMonth() + 1);
        const day = this.formatWithZeroes(date.getDate());
        return {
            hidden: `${year}-${month}-${day}`,
            visible: `${day}-${month}-${year}`
        };
    };
}

export default new FluidFormBackend();
