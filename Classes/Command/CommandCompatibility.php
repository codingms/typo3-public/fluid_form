<?php

declare(strict_types=1);

namespace CodingMs\FluidForm\Command;

use Doctrine\DBAL\Result;
use ReflectionMethod;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if ((new ReflectionMethod(Command::class, 'execute'))->hasReturnType()) {
    /** @internal */
    trait CommandCompatibility
    {
        protected function execute(InputInterface $input, OutputInterface $output): int
        {
            $this->io = new SymfonyStyle($input, $output);
            $this->io->title($this->getDescription());

            $subject = $input->getOption('subject');
            $senderEmail = $input->getOption('senderEmail');
            $receiverEmail = $input->getOption('receiverEmail');
            $delete = strtolower($input->getOption('delete')) === 'yes';

            if (empty($receiverEmail)) {
                $this->log('No receiver email given (option "receiverEmail"). Please specify a receiver email!', 'error');
                return 1;
            }

            $this->senderEmail = [$senderEmail => $senderEmail];
            $this->receiverEmail = [$receiverEmail => $receiverEmail];
            $data = [];

            $formKeys = $input->getOption('formKeys');
            $formKeys = GeneralUtility::trimExplode(',', $formKeys, true);
            if (count($formKeys) === 0) {
                $this->log('No form keys given (option "formKeys"). Please enter one or more form keys!', 'error');
                return 1;
            }

            foreach ($formKeys as $formKey) {
                /** @var ConnectionPool $connectionPool */
                $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
                $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_fluidform_domain_model_form');
                $queryBuilder->select('uid', 'form_key', 'form_uid', 'unique_id', 'crdate')
                    ->from('tx_fluidform_domain_model_form')
                    ->where(
                        $queryBuilder->expr()->eq(
                            'form_key',
                            $queryBuilder->createNamedParameter($formKey, PDO::PARAM_STR)
                        )
                    )
                    ->orderBy('uid', 'ASC');
                /** @var Result $formResource */
                $formResource = $queryBuilder->execute();
                $data[$formKey] = [];
                while ($formRow = $formResource->fetch(PDO::FETCH_ASSOC)) {
                    $dataRow = [
                        'form_key' => $formRow['form_key'],
                        'form_uid' => $formRow['form_uid'],
                        'mail_uid' => $formRow['uid'],
                        'unique_id' => $formRow['unique_id'],
                        'date' => date(DATE_ATOM, $formRow['crdate']),
                    ];
                    $data[$formKey]['title'] = [
                        'form_key' => 'Form-Key',
                        'form_uid' => 'Form-Uid',
                        'mail_uid' => 'Mail-Uid',
                        'unique_id' => 'Unique-Id',
                        'date' => 'Date',
                    ];
                    $data[$formKey]['types'] = [
                        'form_key' => 'string',
                        'form_uid' => 'string',
                        'mail_uid' => 'int',
                        'unique_id' => 'string',
                        'date' => 'string',
                    ];
                    $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_fluidform_domain_model_field');
                    $queryBuilder->select('*')
                        ->from('tx_fluidform_domain_model_field')
                        ->where('fluidform = ' . $formRow['uid']);
                    /** @var Result $fieldResource */
                    $fieldResource = $queryBuilder->execute();
                    while ($fieldRow = $fieldResource->fetch(PDO::FETCH_ASSOC)) {
                        $dataRow[$fieldRow['field_key']] = $fieldRow['field_value'];
                        $data[$formKey]['title'][$fieldRow['field_key']] = $fieldRow['field_label'];
                        $data[$formKey]['types'][$fieldRow['field_key']] = $fieldRow['field_type'];
                    }
                    $data[$formKey]['items'][] = $dataRow;
                }
                if (isset($data[$formKey]['items'])) {
                    if (count($data[$formKey]['items']) > 0) {
                        $this->sendMail($formKey, $subject, $data[$formKey]);
                        if ($delete && isset($data[$formKey]['items'])) {
                            $this->deleteFormData($data[$formKey]['items']);
                        }
                    }
                } else {
                    $this->log('No items found for formKey "' . $formKey . '".', 'notice');
                }
            }
            return 0;
        }
    }
} else {
    /** @internal */
    trait CommandCompatibility
    {
        /**
         * {@inheritDoc}
         *
         * @return int
         */
        /**
         * Executes the command for showing sys_log entries
         *
         * @param InputInterface $input
         * @param OutputInterface $output
         * @return int 0 if everything went fine, or an exit code
         */
        protected function execute(InputInterface $input, OutputInterface $output)
        {
            $this->io = new SymfonyStyle($input, $output);
            $this->io->title($this->getDescription());

            $subject = $input->getOption('subject');
            $senderEmail = $input->getOption('senderEmail');
            $receiverEmail = $input->getOption('receiverEmail');
            $delete = strtolower($input->getOption('delete')) === 'yes';

            if (empty($receiverEmail)) {
                $this->log('No receiver email given (option "receiverEmail"). Please specify a receiver email!', 'error');
                return 1;
            }

            $this->senderEmail = [$senderEmail => $senderEmail];
            $this->receiverEmail = [$receiverEmail => $receiverEmail];
            $data = [];

            $formKeys = $input->getOption('formKeys');
            $formKeys = GeneralUtility::trimExplode(',', $formKeys, true);
            if (count($formKeys) === 0) {
                $this->log('No form keys given (option "formKeys"). Please enter one or more form keys!', 'error');
                return 1;
            }

            foreach ($formKeys as $formKey) {
                /** @var ConnectionPool $connectionPool */
                $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
                $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_fluidform_domain_model_form');
                $queryBuilder->select('uid', 'form_key', 'form_uid', 'unique_id', 'crdate')
                    ->from('tx_fluidform_domain_model_form')
                    ->where(
                        $queryBuilder->expr()->eq(
                            'form_key',
                            $queryBuilder->createNamedParameter($formKey, PDO::PARAM_STR)
                        )
                    )
                    ->orderBy('uid', 'ASC');
                /** @var Result $formResource */
                $formResource = $queryBuilder->execute();
                $data[$formKey] = [];
                while ($formRow = $formResource->fetch(PDO::FETCH_ASSOC)) {
                    $dataRow = [
                        'form_key' => $formRow['form_key'],
                        'form_uid' => $formRow['form_uid'],
                        'mail_uid' => $formRow['uid'],
                        'unique_id' => $formRow['unique_id'],
                        'date' => date(DATE_ATOM, $formRow['crdate']),
                    ];
                    $data[$formKey]['title'] = [
                        'form_key' => 'Form-Key',
                        'form_uid' => 'Form-Uid',
                        'mail_uid' => 'Mail-Uid',
                        'unique_id' => 'Unique-Id',
                        'date' => 'Date',
                    ];
                    $data[$formKey]['types'] = [
                        'form_key' => 'string',
                        'form_uid' => 'string',
                        'mail_uid' => 'int',
                        'unique_id' => 'string',
                        'date' => 'string',
                    ];
                    $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_fluidform_domain_model_field');
                    $queryBuilder->select('*')
                        ->from('tx_fluidform_domain_model_field')
                        ->where('fluidform = ' . $formRow['uid']);
                    /** @var Result $fieldResource */
                    $fieldResource = $queryBuilder->execute();
                    while ($fieldRow = $fieldResource->fetch(PDO::FETCH_ASSOC)) {
                        $dataRow[$fieldRow['field_key']] = $fieldRow['field_value'];
                        $data[$formKey]['title'][$fieldRow['field_key']] = $fieldRow['field_label'];
                        $data[$formKey]['types'][$fieldRow['field_key']] = $fieldRow['field_type'];
                    }
                    $data[$formKey]['items'][] = $dataRow;
                }
                if (isset($data[$formKey]['items'])) {
                    if (count($data[$formKey]['items']) > 0) {
                        $this->sendMail($formKey, $subject, $data[$formKey]);
                        if ($delete && isset($data[$formKey]['items'])) {
                            $this->deleteFormData($data[$formKey]['items']);
                        }
                    }
                } else {
                    $this->log('No items found for formKey "' . $formKey . '".', 'notice');
                }
            }
            return 0;
        }
    }
}
