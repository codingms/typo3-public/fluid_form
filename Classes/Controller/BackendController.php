<?php

namespace CodingMs\FluidForm\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2020 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\FluidForm\Domain\Repository\FormRepository;
use CodingMs\FluidForm\Service\BackendService;
use CodingMs\FluidForm\Service\TypoScriptService;
use CodingMs\Modules\Controller\BackendController as BackendBaseController;
use CodingMs\Modules\Utility\BackendListUtility;
use Exception;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Backend\Routing\UriBuilder as UriBuilderBackend;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Page\JavaScriptModuleInstruction;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * BackendController
 */
class BackendController extends BackendBaseController
{
    /**
     * @var FormRepository
     */
    protected $formRepository;

    /**
     * @var BackendService
     */
    protected $backendService;

    public function __construct(
        \TYPO3\CMS\Core\TypoScript\TypoScriptService $typoScriptService,
        BackendListUtility $backendListUtility,
        UriBuilderBackend $uriBuilderBackend,
        ModuleTemplateFactory $moduleTemplateFactory,
        FormRepository $formRepository,
        BackendService $backendService
    ) {
        parent::__construct(
            $typoScriptService,
            $backendListUtility,
            $uriBuilderBackend,
            $moduleTemplateFactory
        );
        $this->extensionName = 'FluidForm';
        $this->moduleName = 'fluidform_fluidform';
        $this->modulePrefix = 'tx_fluidform_fluidform';
        if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
            $this->moduleName = 'web_FluidFormFluidform';
            $this->modulePrefix = 'tx_fluidform_web_fluidform';
        }
        //
        $this->backendService = $backendService;
        $this->backendListUtility = $backendListUtility;
        $this->formRepository = $formRepository;
        //
        // Write settings in global array, in order
        // to have formatting options in domain models as well
        $GLOBALS['EXTENSIONS']['fluidform']['settings'] = $this->settings;
    }

    /**
     * @throws Exception
     */
    protected function initializeAction()
    {
        parent::initializeAction();
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        if ((int)VersionNumberUtility::getCurrentTypo3Version() > 11) {
            $pageRenderer->getJavaScriptRenderer()->addJavaScriptModuleInstruction(
                JavaScriptModuleInstruction::create('@codingms/fluid-form/FluidFormBackend.js')->invoke('initialize')
            );
        } else {
            $pageRenderer->loadRequireJsModule('TYPO3/CMS/FluidForm/Backend/FluidFormBackendLegacy', 'function(FluidFormBackendLegacy){FluidFormBackendLegacy.initialize()}');
        }

        $this->createMenu();
        $this->createButtons();
    }

    protected function createMenu()
    {
        $actions = [
            [
                'action' => 'overview',
                'label' => LocalizationUtility::translate('tx_fluidform_label.menu_overview', 'FluidForm')
            ]
        ];
        $this->createMenuActions($actions);
    }

    /**
     * @throws Exception
     */
    protected function createButtons()
    {
        $buttonBar = $this->moduleTemplate->getDocHeaderComponent()->getButtonBar();
        switch ($this->request->getControllerActionName()) {
            case 'overview':
                $this->getButton($buttonBar, 'refresh', [
                    'translationKey' => 'list_overview_mails_refresh'
                ]);
                $this->getButton($buttonBar, 'bookmark', [
                    'translationKey' => 'list_overview_mails_bookmark'
                ]);
                break;
        }
    }

    /**
     * Mail overview
     *
     * @return ResponseInterface
     * @throws NoSuchArgumentException
     * @throws SiteNotFoundException
     */
    public function overviewAction()
    {
        $pages = $this->backendService->getMailsPages();
        if (isset($this->page['module']) && $this->page['module'] !== 'mails') {
            $this->addFlashMessage('Please select a mail container', 'Error', AbstractMessage::ERROR);
            $this->getViewToUse()->assign('pages', $pages);
            $this->getViewToUse()->assign('restrictedContext', true);
        } else {
            //
            // Fetch form configurations for current page
            $typoScript = TypoScriptService::getTypoScript($this->pageUid);
            $forms = $typoScript['plugin']['tx_fluidform']['settings']['forms'];
            //
            // Build list
            $list = $this->backendListUtility->initList(
                $this->settings['lists']['mails'],
                /** @phpstan-ignore-next-line */
                $this->request,
                ['dateFrom', 'dateTo', 'page', 'form', 'searchWord']
            );
            //
            // Selected page
            $list['page']['items'] = $pages;
            $list['page']['selected'] = $this->pageUid;
            if ($this->request->hasArgument('page')) {
                $list['page']['selected'] = (int)$this->request->getArgument('page');
            }
            //
            // Form selection
            foreach (array_keys($forms) as $formKey) {
                $formLabel = GeneralUtility::camelCaseToLowerCaseUnderscored((string)$formKey);
                $list['form']['items'][$formKey] = str_replace('_', ' ', $formLabel);
                if (!isset($list['form']['selected'])) {
                    $list['form']['selected'] = $formKey;
                }
            }
            if ($this->request->hasArgument('form')) {
                $list['form']['selected'] = trim($this->request->getArgument('form'));
            }
            //
            // Date range
            if ($this->request->hasArgument('dateFrom')) {
                $list['dateFrom'] = $this->request->getArgument('dateFrom');
            }
            if (!isset($list['dateFrom']) || trim($list['dateFrom']) === '') {
                $list['dateFrom'] = date('d-m-Y', (time() - 60 * 60 * 24 * 7));
            }
            if ($this->request->hasArgument('dateTo')) {
                $list['dateTo'] = $this->request->getArgument('dateTo');
            }
            if (!isset($list['dateTo']) || trim($list['dateTo']) === '') {
                $list['dateTo'] = date('d-m-Y');
            }
            //
            // Get search word
            if ($this->request->hasArgument('searchWord')) {
                $list['searchWord'] = trim($this->request->getArgument('searchWord'));
            }
            //
            // Store settings
            $this->backendListUtility->writeSettings($list['id'], $list);
            //
            // Remove list columns not in form fields
            $dontRemove = ['creationDate', 'formKey'];
            foreach ($forms[$list['form']['selected']]['fieldsets'] as $fieldset) {
                foreach ($fieldset['fields'] as $fieldKey => $fieldData) {
                    $dontRemove[] = $fieldKey;
                }
            }
            $removedColumns = 0;
            foreach ($list['fields'] as $listFieldKey => $listFieldData) {
                if (!in_array($listFieldKey, $dontRemove)) {
                    $removedColumns++;
                    unset($list['fields'][$listFieldKey]);
                }
            }
            $list['columnsInList'] -= $removedColumns;
            $list['columnsInExport'] -= $removedColumns;
            //
            // Export Result as CSV!?
            if ($this->request->hasArgument('csv')) {
                $list['limit'] = 0;
                $list['offset'] = 0;
                $list['pid'] = $this->pageUid;
                $mails = $this->formRepository->findAllForBackendList($list);
                $list['countAll'] = $this->formRepository->findAllForBackendList($list, true);
                /** @phpstan-ignore-next-line */
                $this->backendListUtility->exportAsCsv($mails, $list);
            } else {
                $list['pid'] = $this->pageUid;
                $mails = $this->formRepository->findAllForBackendList($list);
                $list['countAll'] = $this->formRepository->findAllForBackendList($list, true);
            }
            //
            $this->getViewToUse()->assign('list', $list);
            $this->getViewToUse()->assign('mails', $mails);
            $this->getViewToUse()->assign('actionMethodName', $this->actionMethodName);
        }
        $this->getViewToUse()->assign('currentPage', $this->pageUid);

        return $this->renderViewToUse();
    }
}
