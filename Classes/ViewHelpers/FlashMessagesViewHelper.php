<?php

namespace CodingMs\FluidForm\ViewHelpers;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageRendererResolver;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\RequestInterface;
use TYPO3\CMS\Extbase\Service\ExtensionService;
use TYPO3\CMS\Fluid\Core\Rendering\RenderingContext;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Render flash messages in Bootstrap style.
 * Additionally merge all equal messages into one.
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class FlashMessagesViewHelper extends AbstractTagBasedViewHelper
{
    use CompileWithRenderStatic;

    /**
     * ViewHelper outputs HTML therefore output escaping has to be disabled
     *
     * @var bool
     */
    protected $escapeOutput = false;

    public function initializeArguments(): void
    {
        $this->registerArgument('queueIdentifier', 'string', 'Flash-message queue to use');
        $this->registerArgument('as', 'string', 'The name of the current flashMessage variable for rendering inside');
    }

    /**
     * Renders the flash messages as unordered list
     *
     * @param array<\TYPO3\CMS\Core\Messaging\FlashMessage> $flashMessages
     * @return string
     */
    protected function renderUl(array $flashMessages)
    {
        return $this->renderDiv($flashMessages);
    }

    /**
     * Renders the flash messages as nested divs
     *
     * @param array<\TYPO3\CMS\Core\Messaging\FlashMessage> $flashMessages
     * @return string
     */
    protected function renderDiv(array $flashMessages)
    {
        $this->tag->setTagName('div');
        if ($this->hasArgument('class')) {
            $this->tag->addAttribute('class', $this->arguments['class']);
        } else {
            $this->tag->addAttribute('class', 'typo3-messages');
        }

        $tagContents = [];
        /** @var FlashMessage $singleFlashMessage */
        foreach ($flashMessages as $singleFlashMessage) {
            $severity = $singleFlashMessage->getSeverity()->value;
            if ($severity === FlashMessage::NOTICE) {
                $severityKey = 'info';
            } elseif ($severity === FlashMessage::INFO) {
                $severityKey = 'info';
            } elseif ($severity === FlashMessage::OK) {
                $severityKey = 'success';
            } elseif ($severity === FlashMessage::WARNING) {
                $severityKey = 'warning';
            } else {
                $severityKey = 'danger';
            }

            $title = '<strong class="title">' . $singleFlashMessage->getTitle() . '</strong>';
            $message = '<p class="message">' . $singleFlashMessage->getMessage() . '</p>';

            $tagContents[$severityKey][] = '<div class="message">' . $title . $message . '</div>';
        }

        $tagContent = '';
        $closeButton = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        if (!empty($tagContents['info'])) {
            $tagContent .= '<div class="alert alert-info alert-dismissible" role="alert">';
            $tagContent .= $closeButton . implode('', $tagContents['info']) . '</div>';
        }
        if (!empty($tagContents['success'])) {
            $tagContent .= '<div class="alert alert-success alert-dismissible" role="alert">';
            $tagContent .= $closeButton . implode('', $tagContents['success']) . '</div>';
        }
        if (!empty($tagContents['warning'])) {
            $tagContent .= '<div class="alert alert-warning alert-dismissible" role="alert">';
            $tagContent .= $closeButton . implode('', $tagContents['warning']) . '</div>';
        }
        if (!empty($tagContents['danger'])) {
            $tagContent .= '<div class="alert alert-danger alert-dismissible" role="alert">';
            $tagContent .= $closeButton . implode('', $tagContents['danger']) . '</div>';
        }

        $this->tag->setContent($tagContent);
        return $this->tag->render();
    }

    /**
     * Renders FlashMessages and flushes the FlashMessage queue
     *
     * Note: This does not disable the current page cache in order to prevent FlashMessage output
     *       from being cached.
     *       In case of conditional flash message rendering, caching must be disabled
     *       (e.g. for a controller action).
     *       Custom caching using the Caching Framework can be used in this case.
     *
     * @return mixed
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $as = $arguments['as'];
        $queueIdentifier = $arguments['queueIdentifier'];

        if ($queueIdentifier === null) {
            /** @var RenderingContext $renderingContext */
            $request = $renderingContext->getRequest();
            if (!$request instanceof RequestInterface) {
                // Throw if not an extbase request
                throw new \RuntimeException(
                    'ViewHelper f:flashMessages needs an extbase Request object to resolve the Queue identifier magically.'
                    . ' When not in extbase context, set attribute "queueIdentifier".',
                    1639821269
                );
            }
            $extensionService = GeneralUtility::makeInstance(ExtensionService::class);
            $pluginNamespace = $extensionService->getPluginNamespace($request->getControllerExtensionName(), $request->getPluginName());
            $queueIdentifier = 'extbase.flashmessages.' . $pluginNamespace;
        }

        $flashMessageQueue = GeneralUtility::makeInstance(FlashMessageService::class)->getMessageQueueByIdentifier($queueIdentifier);
        $flashMessages = $flashMessageQueue->getAllMessagesAndFlush();
        if (count($flashMessages) === 0) {
            return '';
        }

        if ($as === null) {
            return GeneralUtility::makeInstance(FlashMessageRendererResolver::class)->resolve()->render($flashMessages);
        }
        $templateVariableContainer = $renderingContext->getVariableProvider();
        $templateVariableContainer->add($as, $flashMessages);
        $content = $renderChildrenClosure();
        $templateVariableContainer->remove($as);

        return $content;
    }
}
