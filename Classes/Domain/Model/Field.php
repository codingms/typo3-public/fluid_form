<?php

namespace CodingMs\FluidForm\Domain\Model;

use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
/**
 * Field
 */
class Field extends AbstractEntity
{
    /**
     * fieldType
     *
     * @var string
     */
    protected $fieldType = '';

    /**
     * fieldLabel
     *
     * @var string
     */
    protected $fieldLabel = '';

    /**
     * fieldKey
     *
     * @var string
     */
    protected $fieldKey = '';

    /**
     * fieldUpload
     *
     * @var \CodingMs\FluidForm\Domain\Model\FileReference
     */
    protected $fieldUpload;

    /**
     * fieldValue
     *
     * @var string
     */
    protected $fieldValue = '';

    /**
     * fieldText
     *
     * @var string
     */
    protected $fieldText = '';

    /**
     * Returns the fieldType
     *
     * @return string $fieldType
     */
    public function getFieldType()
    {
        return $this->fieldType;
    }

    /**
     * @param string $fieldType
     */
    public function setFieldType(string $fieldType)
    {
        $this->fieldType = $fieldType;
    }

    /**
     * @return string
     */
    public function getFieldLabel()
    {
        return $this->fieldLabel;
    }

    /**
     * @param string $fieldLabel
     */
    public function setFieldLabel(string $fieldLabel)
    {
        $this->fieldLabel = $fieldLabel;
    }

    /**
     * @return string
     */
    public function getFieldKey()
    {
        return $this->fieldKey;
    }

    /**
     * @param string $fieldKey
     */
    public function setFieldKey(string $fieldKey)
    {
        $this->fieldKey = $fieldKey;
    }

    /**
     * Returns the fieldUpload
     *
     * @return \CodingMs\FluidForm\Domain\Model\FileReference $fieldUpload
     */
    public function getFieldUpload()
    {
        return $this->fieldUpload;
    }

    /**
     * @param \CodingMs\FluidForm\Domain\Model\FileReference $fieldUpload
     */
    public function setFieldUpload(FileReference $fieldUpload)
    {
        $this->fieldUpload = $fieldUpload;
    }

    /**
     * Returns the fieldValue
     *
     * @return string $fieldValue
     */
    public function getFieldValue()
    {
        return $this->fieldValue;
    }

    /**
     * @param string $fieldValue
     */
    public function setFieldValue(string $fieldValue)
    {
        $this->fieldValue = $fieldValue;
    }

    /**
     * Returns the fieldText
     *
     * @return string $fieldText
     */
    public function getFieldText()
    {
        return $this->fieldText;
    }

    /**
     * @param string $fieldText
     */
    public function setFieldText(string $fieldText)
    {
        $this->fieldText = $fieldText;
    }
}
