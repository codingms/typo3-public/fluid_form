<?php

declare(strict_types=1);

namespace CodingMs\FluidForm\Domain\Session;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;

/**
 * Session handling
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class SessionHandler implements SingletonInterface
{
    /**
     * Return stored session data
     * @param string $extension Extension-Key
     * @return array<string, mixed> The stored data
     */
    public function restoreFromSession(string $extension = 'fluid_form'): array
    {
        $sessionData = [];
        $frontendUser = $this->getFrontendUser();
        if (isset($frontendUser)) {
            $sessionDataString = $frontendUser->getKey('ses', 'tx_' . $extension);
            if (is_string($sessionDataString)) {
                $sessionDataRestored = unserialize($sessionDataString);
                if (is_array($sessionDataRestored)) {
                    $sessionData = $sessionDataRestored;
                }
            }
        }
        return $sessionData;
    }

    /**
     * Write session data
     * @param array<string, mixed> $data any serializable object to store into the session
     * @param string $extension Extension-Key
     * @return SessionHandler This object
     */
    public function writeToSession(array $data, string $extension = 'fluid_form'): SessionHandler
    {
        $frontendUser = $this->getFrontendUser();
        if (isset($frontendUser)) {
            $sessionData = serialize($data);
            $frontendUser->setKey('ses', 'tx_' . $extension, $sessionData);
            $frontendUser->storeSessionData();
        }
        return $this;
    }

    /**
     * Clean up session
     * @param string $extension Extension-Key
     * @return SessionHandler This object
     */
    public function cleanUpSession(string $extension = 'fluid_form'): SessionHandler
    {
        $frontendUser = $this->getFrontendUser();
        if (isset($frontendUser)) {
            $frontendUser->setKey('ses', 'tx_' . $extension, null);
            $frontendUser->storeSessionData();
        }
        return $this;
    }

    /**
     * @return FrontendUserAuthentication|null
     */
    protected function getFrontendUser(): ?FrontendUserAuthentication
    {
        return $GLOBALS['TSFE']->fe_user;
    }
}
